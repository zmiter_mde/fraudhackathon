package android.test.mde.com.testapp.entity;

/**
 * Created by Zmiter on 18.09.2015.
 */
import java.util.Date;

public class StocksContainer {
    private double priceClose;
    private Date date;
    private double sma;
    private double upBoll;
    private double lowBoll;

    public StocksContainer(double priceClose, Date date, double sma, double upBoll, double lowBoll) {
        this.priceClose = priceClose;
        this.date = date;
        this.sma = sma;
        this.upBoll = upBoll;
        this.lowBoll = lowBoll;
    }

    public double getPriceClose() {
        return priceClose;
    }

    public void setPriceClose(double priceClose) {
        this.priceClose = priceClose;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getSma() {
        return sma;
    }

    public void setSma(double sma) {
        this.sma = sma;
    }

    public double getUpBoll() {
        return upBoll;
    }

    public void setUpBoll(double upBoll) {
        this.upBoll = upBoll;
    }

    public double getLowBoll() {
        return lowBoll;
    }

    public void setLowBoll(double lowBoll) {
        this.lowBoll = lowBoll;
    }

    @Override
    public String toString() {
        return "StocksContainer{" +
                "priceClose=" + priceClose +
                ", date=" + date +
                ", sma=" + sma +
                ", upBoll=" + upBoll +
                ", lowBoll=" + lowBoll +
                '}';
    }
}
