package android.test.mde.com.testapp.service;

import android.os.AsyncTask;
import android.os.StrictMode;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

/**
 * Created by Zmiter on 18.09.2015.
 */
class RetrieveFeedTask extends AsyncTask<String, Void, String> {

    private Exception exception;

    public Exception getException() {
        return exception;
    }

    @Override
    protected String doInBackground(String... params) {

        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
        try {
            URL url= new URL(params[0]);
            InputStream inputStream = url.openStream();
            Scanner scan = new Scanner(inputStream);
            int max = 100, count = 0;
            StringBuilder info = new StringBuilder();
            while (scan.hasNext() && ++count < max) {
                info.append(scan.next());
            }

            scan.close();
            inputStream.close();

            return info.toString();
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }
}