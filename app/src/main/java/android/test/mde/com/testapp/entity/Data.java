package android.test.mde.com.testapp.entity;

import java.io.Serializable;

/**
 * Created by zmiter on 19.09.2015.
 */
public class Data implements Serializable {

    private int amount;

    public Data(int amount) {
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Amount = " + amount;
    }
}
