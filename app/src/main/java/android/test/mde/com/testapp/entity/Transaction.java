package android.test.mde.com.testapp.entity;

import java.io.Serializable;

/**
 * Created by zmiter on 19.09.2015.
 */
public class Transaction implements Serializable {

    private String trId;
    private String bic;
    private String cardnum;
    private int amount;
    private String currency;
    private int mcc;
    private String country;

    public Transaction(String trId, String bic, String cardnum, int amount, String currency, int mcc, String country) {
        this.trId = trId;
        this.bic = bic;
        this.cardnum = cardnum;
        this.amount = amount;
        this.currency = currency;
        this.mcc = mcc;
        this.country = country;
    }

    public String getTrId() {
        return trId;
    }

    public void setTrId(String trId) {
        this.trId = trId;
    }

    public String getBic() {
        return bic;
    }

    public void setBic(String bic) {
        this.bic = bic;
    }

    public String getCardnum() {
        return cardnum;
    }

    public void setCardnum(String cardnum) {
        this.cardnum = cardnum;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return trId + " " + bic + " " +
                cardnum + " " + amount + " " +
                currency + " " + mcc + " " +
                country;
    }

}
