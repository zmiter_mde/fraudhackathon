package android.test.mde.com.testapp.entity;

/**
 * Created by Zmiter on 18.09.2015.
 */
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Gabriel on 9/12/2015.
 */
public class MovingAverage {

    private final Queue<Double> window = new LinkedList<Double>();
    private final int PERIOD = 20;
    private double sum;

    public void newNum(double num) {
        sum += num;
        window.add(num);
        if (window.size() > PERIOD) {
            sum -= window.remove();
        }
    }

    public double getAvg() {
        if (window.isEmpty()) return 0; // technically the average is undefined
        return sum / window.size();
    }

}
