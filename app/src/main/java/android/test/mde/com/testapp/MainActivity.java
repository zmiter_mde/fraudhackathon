package android.test.mde.com.testapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.test.mde.com.testapp.adapter.StableArrayAdapter;
import android.test.mde.com.testapp.entity.Alert;
import android.test.mde.com.testapp.entity.Data;
import android.test.mde.com.testapp.entity.MovingAverage;
import android.test.mde.com.testapp.entity.Rule;
import android.test.mde.com.testapp.entity.StocksContainer;
import android.test.mde.com.testapp.entity.Transaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.math3.stat.descriptive.moment.StandardDeviation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.csv.*;

import org.json.simple.parser.ParseException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class MainActivity extends ActionBarActivity {

    public static final String CONTEXT_ROOT = "https://fraudster1909.herokuapp.com/";
    private static final String DATE_FORMAT = "HH:mm:ss.SSSZ";

    private static final String TRANSACTIONS_RETRIEVED = "Transactions retrieved";
    private static final String ALERTS_RETRIEVED = "Alerts retrieved";

    private static final String [] FILE_HEADER_MAPPING = {
            "Date", "Open", "High", "Low", "Close", "Volume", "Adj Close"
    };

    private static final String TR_ID = "trid";
    private static final String DATA = "data";
    private static final String RULES = "rules";
    private static final String TRANSACTION = "transaction";
    private static final String BIC = "bic";
    private static final String CARDNUM = "cardnum";
    private static final String AMOUNT = "amount";
    private static final String CURRENCY = "currency";
    private static final String MCC = "mcc";
    private static final String COUNTRY = "country";

    private static final JSONParser parser = new JSONParser();

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private ListView listView;

    private ArrayList<String> list = new ArrayList<String>();;
    private StableArrayAdapter adapter;
    private List<Transaction> transactions;
    public static List<Alert> alerts = new LinkedList<>();

    private Timer timer = new Timer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);

        listView = (ListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);

        timer.scheduleAtFixedRate(new TimerTask() {

            synchronized public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.i("zmiter", "info from timer");
                        setMode();
                        try {
                            List<Transaction> temp = getTransactions(new GregorianCalendar(2015, 0, 1).getTime());
                            if (temp.size() > 0)
                                setTransactions(temp);
                            setViewChanges(true);

                            alerts = getAlerts();
                            /*
                            if (alerts.size() > 0)
                                createNotification(alerts);
                                */

                        } catch (IOException e) {
                            e.printStackTrace();
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        }, 1000, 1000);
    }

    public void createNotification(List<Alert> alerts) {
        // Prepare intent which is triggered if the
        // notification is selected
        Intent intent = new Intent(this, NotificationReceiverActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        intent.putExtra("alertCount", alerts.size());
        intent.setAction("" + Math.random());
        /*
        for (int i = 0; i < alerts.size(); ++i)
            intent.putExtra("alert" + i, alerts.get(i)); //Put your id to your next Intent
        */
        // Build notification
        // Actions are just fake
        Notification noti = new Notification.Builder(this)
                .setContentTitle("Alerts retrieved")
                .setContentText("You've got new alerts").setSmallIcon(R.drawable.notification_template_icon_bg)
                .setContentIntent(pIntent)
                .addAction(R.drawable.notification_template_icon_bg, "Call", pIntent)
                /*
                .addAction(R.drawable.notification_template_icon_bg, "More", pIntent)
                .addAction(R.drawable.notification_template_icon_bg, "And more", pIntent)
                */
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        noti.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, noti);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void setMode() {
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public void getAllTransactions(View v) throws Exception {
        setMode();

        setTransactions(getTransactions(new GregorianCalendar(2015, 0, 1).getTime()));

        setViewChanges(true);
    }

    private void setTransactions(List<Transaction> temp) throws IOException, ParseException {
        transactions = temp;
        List<String> newVals = new LinkedList<>();
        for (Transaction transaction : transactions)
            newVals.add(transaction.toString());
        List<String> values = new ArrayList<String>(newVals);
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, values);
    }

    private void setViewChanges(boolean isTransaction) {
        ListView visitType = (ListView)this.findViewById(R.id.listView);
        visitType.setAdapter(adapter);
        TextView text = (TextView)this.findViewById(R.id.textView);
        text.setText(isTransaction ? TRANSACTIONS_RETRIEVED : ALERTS_RETRIEVED);
    }

    public void getAllAlerts(View v) throws Exception {

        setMode();

        int trId = 0;
        setAlerts(trId);

        setViewChanges(false);
    }

    private void setAlerts(int trId) throws IOException, ParseException {

        List<Alert> alerts  = getAlerts();

        String[] newVals = {
                alerts.get(0).toString(),
                alerts.get(1).toString()
        };
        List<String> values = new ArrayList<String>(Arrays.asList(newVals));
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, values);
    }

    private List<Alert> getAlerts() throws IOException, ParseException {
        String url = getAlertsURL();
        String jsonString = getJSONString(url);
        List<Alert> alerts = parseAlerts(jsonString);
        return alerts;
    }

    private String getAlertsURL(int trId) {
        return CONTEXT_ROOT + "alerts?trid=" + trId;
    }

    private String getAlertsURL() {
        return CONTEXT_ROOT + "alerts";
    }

    private List<Transaction> getTransactions(Date start) throws IOException, ParseException {
        String url = getTransactionsURL(start);
        String jsonString = getJSONString(url, true);
        List<Transaction> res = parseTransactions(jsonString);
        return res;
    }

    private String getTransactionsURL(Date start) {
        return CONTEXT_ROOT + "/feed";
                //?time=" + dateToString(start);
    }

    private String dateToString(Date start) {
        DateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date today = Calendar.getInstance().getTime();
        return df.format(today);
    }

    private static int transactionsCount = 0;

    private static String getJSONString(String url, boolean transactionOrAlert) throws IOException {

        //url = constructURL("GOOG", new GregorianCalendar(2015, 0, 1), new GregorianCalendar(2015, 8, 31));
        if (!transactionOrAlert) {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            InputStream inputStream = connection.getInputStream();
            Scanner scan = new Scanner(inputStream);
            StringBuilder res = new StringBuilder();
            while (scan.hasNext()) {
                res.append(scan.next());
            }
            return res.toString();
        } else {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            InputStream inputStream = connection.getInputStream();
            Scanner scan = new Scanner(inputStream);
            StringBuilder res = new StringBuilder();
            while (scan.hasNext()) {
                res.append(scan.next());
            }
            return res.toString();
            /*
            StringBuilder res = new StringBuilder();
            res.append("[{\"trid\":\"81253348\",\"bic\":\"DABAIE2D\",\"cardnum\":\"4945-33XX-XXXX-8125\",\"amount\":500,\"currency\":\"USD\",\"mcc\":255,\"country\":\"US\"},{\"trid\":\"81253349\",\"bic\":\"DABAIE2D\",\"cardnum\":\"4945-33XX-XXXX-9999\",\"amount\":1000,\"currency\":\"GBP\",\"mcc\":255,\"country\":\"UK\"}]");

            transactionsCount = (transactionsCount + 1) % 16;

            return res.toString();
            */
        }
    }

    private static String getJSONString(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        InputStream inputStream = connection.getInputStream();
        Scanner scan = new Scanner(inputStream);
        StringBuilder res = new StringBuilder();
        while (scan.hasNext()) {
            res.append(scan.next());
        }
        return res.toString();
/*
        StringBuilder res = new StringBuilder();
        if (transactionOrAlert) {
            res.append("[");
            for (int i = 0; i < transactionsCount - 1; ++i) {
                res.append("{\"trid\": \"" + i + "\", \"data\": { \"amount\":\"" + i + "\"} }");
                if (i < transactionsCount - 1)
                    res.append(",");
            }
            res.append("]");
            transactionsCount = (transactionsCount + 1) % 16;
        } else {
            res.append("[{\"trid\": \"" + trId + "\", \"rules\": [\"First rule\", \"Second rule\"] }");
        }
        return res.toString();
*/
    }

    private List<Transaction> parseTransactions(String jsonString) throws ParseException {

        transactions = new LinkedList<>();

        JSONArray transactionJSON = (JSONArray)parser.parse(jsonString);

        for (int i = 0; i < transactionJSON.size(); ++i) {
            JSONObject oneTransactionJSON = (JSONObject)transactionJSON.get(i);
            transactions.add(getTransactionFromJSON(oneTransactionJSON));
        }

        return transactions;
    }

    private List<Alert> parseAlerts(String jsonString) throws ParseException {

        List<Alert> alerts = new LinkedList<>();

        JSONArray alertJSON = (JSONArray)parser.parse(jsonString);

        for (int i = 0; i < alertJSON.size(); ++i) {
            JSONObject oneAlertJSON = (JSONObject)alertJSON.get(i);

            JSONObject transactionJSON = (JSONObject)oneAlertJSON.get(TRANSACTION);

            Transaction transaction = getTransactionFromJSON(transactionJSON);

            JSONArray rule = (JSONArray)oneAlertJSON.get(RULES);

            List<Rule> rules = new LinkedList<>();
            for (int j = 0; j < rule.size(); ++j)
                rules.add(new Rule(rule.get(j).toString()));

            alerts.add(new Alert(transaction, rules));
        }

        return alerts;
    }

    public Transaction getTransactionFromJSON(JSONObject jsonObject) {
        String trId = jsonObject.get(TR_ID).toString();
        String bic = jsonObject.get(BIC).toString();
        String cardnum = jsonObject.get(CARDNUM).toString();
        int amount = Integer.parseInt(jsonObject.get(AMOUNT).toString());
        String currency = jsonObject.get(CURRENCY).toString();
        int mcc = Integer.parseInt(jsonObject.get(MCC).toString());
        String country = jsonObject.get(COUNTRY).toString();
        return new Transaction(trId, bic, cardnum, amount, currency, mcc, country);
    }



    public List<StocksContainer> getStocks(String symbol, Calendar start, Calendar end) throws Exception {
        MovingAverage avg = new MovingAverage();
        List<StocksContainer> stockList = new ArrayList<StocksContainer>();
        StandardDeviation standardDeviation =  new StandardDeviation();

        String url = constructURL(symbol, start, end);

        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        InputStream inputStream = connection.getInputStream();
        BufferedReader rd = new BufferedReader(
                new InputStreamReader(inputStream));
        CSVParser csvParser = CSVFormat.EXCEL.withHeader(FILE_HEADER_MAPPING).parse(rd);
        List<CSVRecord> records = csvParser.getRecords();

        List<Double> priceList = new ArrayList<Double>();
        for (int i = records.size() - 1 ; i >= 0 ; i--) {
            CSVRecord record = records.get(i);

            String rec = record.get("Date");
            if(!rec.equalsIgnoreCase("date")) { // ignoring first 'head' line

                Date date = sdf.parse(rec); //parse stock's date
                double priceClose = Double.parseDouble(record.get("Close")); //parse stock's closePrice
                avg.newNum(priceClose);
                double sma = avg.getAvg(); //calculate moving AVG
                //deviation calculation START
                priceList.add(priceClose);
                if(priceList.size() > 20){ //removed first element from list  after 20 elements was exceeded
                    priceList.remove(0);
                }
                double[] priceArray = new double[priceList.size()];
                for(int j = 0; j < priceList.size(); j++) {
                    priceArray[j] = priceList.get(j);
                }
                double deviation = standardDeviation.evaluate(priceArray, sma); // calculate deviation
                //deviation calculation END

                double upBoll = sma + 2*deviation;
                double lowBoll = sma - 2*deviation;
//                new BigDecimal(priceClose).setScale(2, BigDecimal.ROUND_UP).doubleValue();
                StocksContainer stocksContainer = new StocksContainer(priceClose, date, sma, upBoll, lowBoll);
                stockList.add(stocksContainer);
//                System.out.println("deviation: " + deviation + " " + stocksContainer);
            }
        }
//        System.out.println("Stocks count : " + priceList.size());
//        System.out.println("stockList last: + " + stockList.get(0));
//        System.out.println("stockList first: + " + stockList.get(stockList.size() - 1));
        return stockList;
    }

    public  String constructURL(String symbol, Calendar start, Calendar end) {
        return "http://ichart.finance.yahoo.com/table.csv" +
                "?s=" +
                symbol + "&a=" +
                Integer.toString(start.get(Calendar.MONTH) ) +
                "&b=" +
                start.get(Calendar.DAY_OF_MONTH) + "&c=" +
                Integer.toString(start.get(Calendar.YEAR)) +
                "&d=" +
                Integer.toString(end.get(Calendar.MONTH) ) +
                "&e=" +
                Integer.toString(end.get(Calendar.DAY_OF_MONTH)) + "&f=" +
                Integer.toString(end.get(Calendar.YEAR)) +
                "&g=d&ignore=.csv";
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
