package android.test.mde.com.testapp.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Created by zmiter on 19.09.2015.
 */
public class Alert implements Serializable {

    private Transaction transaction;
    private List<Rule> rules;

    public Alert(Transaction transaction, List<Rule> rules) {
        this.transaction = transaction;
        this.rules = rules;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }

    @Override
    public String toString() {
        StringBuilder rulesStr = new StringBuilder(" ");
        for (Rule rule : rules)
            rulesStr.append(rule.toString()).append(" ");
        return transaction + rulesStr.toString();
    }
}
