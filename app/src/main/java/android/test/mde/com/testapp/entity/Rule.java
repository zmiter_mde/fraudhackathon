package android.test.mde.com.testapp.entity;

import java.io.Serializable;

/**
 * Created by zmiter on 19.09.2015.
 */
public class Rule implements Serializable {

    private String name;

    public Rule(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
