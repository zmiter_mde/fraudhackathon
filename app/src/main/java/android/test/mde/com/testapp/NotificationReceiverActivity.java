package android.test.mde.com.testapp;

/**
 * Created by zmiter on 19.09.2015.
 */
import android.app.Activity;
import android.os.Bundle;
import android.test.mde.com.testapp.adapter.StableArrayAdapter;
import android.test.mde.com.testapp.entity.Alert;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Scanner;

import static android.test.mde.com.testapp.MainActivity.*;

public class NotificationReceiverActivity extends Activity {

    private StableArrayAdapter adapter;

    private ListView listView;

    private ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notified);

        TextView text = (TextView)this.findViewById(R.id.textView2);
        text.setText("Count is " + alerts.size());


        list = new ArrayList<>();
        for (Alert alert : alerts)
            list.add(alert.toString());

        listView = (ListView) findViewById(R.id.listView2);
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    public void blockCard(View view) throws IOException {
        list.clear();
        for (Alert alert : alerts) {
            String url = getBlockURL(alert.getTransaction().getTrId());
            list.add("The card " + alert.getTransaction().getCardnum() + " is " +
                    (blockTheCard(url) ? "" : "not ") + "blocked");
        }
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    private String getBlockURL(String trId) {
        return CONTEXT_ROOT + "blockcard?trid=" + trId;
    }

    private boolean blockTheCard(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        InputStream inputStream = connection.getInputStream();
        Scanner scan = new Scanner(inputStream);
        StringBuilder res = new StringBuilder();
        while (scan.hasNext()) {
            res.append(scan.next());
        }

        return connection.getResponseCode() == 200;
    }

    public void markFraud(View view) throws IOException {
        list.clear();
        for (Alert alert : alerts) {
            String url = getMarkFraudURL(alert.getTransaction().getTrId());
            list.add("The transaction " + alert.getTransaction().getTrId() + " is " +
                    (markFraudTheCard(url) ? "" : "not ") + "marked as fraud");
        }
        adapter = new StableArrayAdapter(this, android.R.layout.simple_list_item_1, list);
        listView.setAdapter(adapter);
    }

    private String getMarkFraudURL(String trId) {
        return CONTEXT_ROOT + "markfraud?trid=" + trId;
    }

    private boolean markFraudTheCard(String url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        InputStream inputStream = connection.getInputStream();
        Scanner scan = new Scanner(inputStream);
        StringBuilder res = new StringBuilder();
        while (scan.hasNext()) {
            res.append(scan.next());
        }
        return connection.getResponseCode() == 200;
    }
}